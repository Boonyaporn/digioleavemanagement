package th.co.digioleavemanagement;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;


public class ProfileActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    BottomNavigationView bottomNavigationView;
    ImageView absent_request;

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        absent_request = findViewById(R.id.absent_request);

        absent_request.setOnClickListener(this);

        SettingMenu();

    }

    public void SettingMenu() {
        bottomNavigationView = findViewById(R.id.nev_bar);
        bottomNavigationView.setItemIconTintList(null);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.profile_nev);
        menuItem.setIcon(getResources().getDrawable(R.drawable.profile_onclick));
        menuItem.setEnabled(true);


        if (RoleDemo.getInstance().getRole().equals("HR")) {
            bottomNavigationView.getMenu().removeItem(R.id.myTeam_nev);
        } else if (RoleDemo.getInstance().getRole().equals("TeamLead")) {
            bottomNavigationView.getMenu().removeItem(R.id.search_employee_nev);
        } else {
            bottomNavigationView.getMenu().removeItem(R.id.search_employee_nev);
            bottomNavigationView.getMenu().removeItem(R.id.notification_nev);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.dashBoard_nev:
                Intent intent = new Intent(ProfileActivity.this, DashBoardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case R.id.search_employee_nev:
                Intent intent5 = new Intent(ProfileActivity.this, SearchEmployeeActivity.class);
                intent5.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent5);
                break;
            case R.id.myTeam_nev:
                Intent intent2 = new Intent(ProfileActivity.this, MyTeamActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent2);
                break;
            case R.id.notification_nev:
                Intent intent3 = new Intent(ProfileActivity.this, RequestNotificationActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent3);
                break;
            case R.id.profile_nev:
                Intent intent4 = new Intent(ProfileActivity.this, ProfileActivity.class);
                intent4.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent4);
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.absent_request:
                startActivity(new Intent(this, AbsentRequestActivity.class));
                break;
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DataLeaveFragment(),"DATA");
        adapter.addFragment(new GraphLeaveFragment(),"GRAPH");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}


