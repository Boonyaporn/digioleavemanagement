package th.co.digioleavemanagement;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class AbsentRequestActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt_submit;
    Spinner spinner_leave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absent_request);

        bt_submit = findViewById(R.id.bt_submit);
        spinner_leave = findViewById(R.id.spinner_leave);

        ArrayAdapter adapter_leave = ArrayAdapter.createFromResource(this,R.array.leave_type,R.layout.my_spinner);
        spinner_leave.setAdapter(adapter_leave);

        bt_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_submit:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
        }
    }
}
