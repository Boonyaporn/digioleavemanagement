package th.co.digioleavemanagement;

public class RoleDemo {

    String role;

    private static final RoleDemo instance = new RoleDemo();

    public RoleDemo() {
        this.role = role;
    }

    public static RoleDemo getInstance() {
        return instance;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
