package th.co.digioleavemanagement;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class RequestNotificationActivity extends AppCompatActivity implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView bottomNavigationView;
    ImageView noti_sick_card , noti_person_card,noti_vaca_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_notification);

        SettingMenu();

        noti_sick_card = findViewById(R.id.noti_sick_card);
        noti_person_card = findViewById(R.id.noti_person_card);
        noti_vaca_card = findViewById(R.id.noti_vaca_card);

        noti_sick_card.setOnClickListener(this);
        noti_person_card.setOnClickListener(this);
        noti_vaca_card.setOnClickListener(this);

    }

    public void SettingMenu(){
        bottomNavigationView = findViewById(R.id.nev_bar);
        bottomNavigationView.setItemIconTintList(null);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem =   menu.findItem(R.id.notification_nev);
        menuItem.setIcon(getResources().getDrawable(R.drawable.notification_onclick));
        menuItem.setEnabled(true);



        if (RoleDemo.getInstance().getRole().equals("HR")){
            bottomNavigationView.getMenu().removeItem(R.id.myTeam_nev);
        }
        else if (RoleDemo.getInstance().getRole().equals("TeamLead")){
            bottomNavigationView.getMenu().removeItem(R.id.search_employee_nev);
        }
        else {
            bottomNavigationView.getMenu().removeItem(R.id.search_employee_nev);
            bottomNavigationView.getMenu().removeItem(R.id.notification_nev);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.noti_sick_card:
                showDialogApprove();
                break;
            case R.id.noti_person_card:
                showDialogApprove();
                break;
            case R.id.noti_vaca_card:
                showDialogApprove();
                break;
        }
    }

    public void showDialogApprove(){
        final Dialog dialog = new Dialog(RequestNotificationActivity.this);
        dialog.setContentView(R.layout.dialog_approve);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView bt_approve = dialog.findViewById(R.id.bt_approve);
        TextView bt_decline = dialog.findViewById(R.id.bt_decline);

        bt_approve.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        bt_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogDecline();
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public void showDialogDecline(){
        final Dialog dialog = new Dialog(RequestNotificationActivity.this);
        dialog.setContentView(R.layout.dialog_decline);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView bt_decline = dialog.findViewById(R.id.bt_decline);

        bt_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case R.id.dashBoard_nev:
                Intent intent = new Intent(RequestNotificationActivity.this, DashBoardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case R.id.search_employee_nev:
                Intent intent5 = new Intent(RequestNotificationActivity.this, SearchEmployeeActivity.class);
                intent5.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent5);
                break;
            case R.id.myTeam_nev:
                Intent intent2 = new Intent(RequestNotificationActivity.this, MyTeamActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent2);
                break;
            case R.id.notification_nev:
                Intent intent3 = new Intent(RequestNotificationActivity.this, RequestNotificationActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent3);
                break;
            case R.id.profile_nev:
                Intent intent4 = new Intent(RequestNotificationActivity.this, ProfileActivity.class);
                intent4.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent4);
                break;
        }

        return false;
    }
}
