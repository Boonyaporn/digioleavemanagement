package th.co.digioleavemanagement;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

public class DashBoardActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    BottomNavigationView bottomNavigationView;

    ImageView leadder , staff1 ,staff2 ,staff3 ,staff4;
    String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        SettingMenu();
        into();

//        leadder.setOnClickListener(this);
        staff1.setOnClickListener(this);
        staff2.setOnClickListener(this);
        staff3.setOnClickListener(this);
    }

    public void into (){
        leadder = findViewById(R.id.leadder);
        staff1 = findViewById(R.id.staff11);
        staff2 = findViewById(R.id.staff22);
        staff3 = findViewById(R.id.staff33);

    }


    public void SettingMenu(){
        bottomNavigationView = findViewById(R.id.nev_bar);
        bottomNavigationView.setItemIconTintList(null);


        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem =   menu.findItem(R.id.dashBoard_nev);
        menuItem.setIcon(getResources().getDrawable(R.drawable.calendar_onclick));
        menuItem.setEnabled(true);


        if (RoleDemo.getInstance().getRole().equals("HR")){
            bottomNavigationView.getMenu().removeItem(R.id.myTeam_nev);
        }
        else if (RoleDemo.getInstance().getRole().equals("TeamLead")){
            bottomNavigationView.getMenu().removeItem(R.id.search_employee_nev);
        }
        else {
            bottomNavigationView.getMenu().removeItem(R.id.search_employee_nev);
            bottomNavigationView.getMenu().removeItem(R.id.notification_nev);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case R.id.dashBoard_nev:
                Intent intent = new Intent(DashBoardActivity.this, DashBoardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case R.id.search_employee_nev:
                Intent intent5 = new Intent(DashBoardActivity.this, SearchEmployeeActivity.class);
                intent5.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent5);
                break;
            case R.id.myTeam_nev:
                Intent intent2 = new Intent(DashBoardActivity.this, MyTeamActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent2);
                break;
            case R.id.notification_nev:
                Intent intent3 = new Intent(DashBoardActivity.this, RequestNotificationActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent3);
                break;
            case R.id.profile_nev:
                Intent intent4 = new Intent(DashBoardActivity.this, ProfileActivity.class);
                intent4.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent4);
                break;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.staff11:
                status = "0";
                if (status == "0") {
                    Dialog("Vacation Leave", "Staff1", "23/1/24", "I am IronMan");
                } else {
                    Dialogworker("Leader");
                }
                break;
            case R.id.staff22:
                status = "0";
                if (status == "0") {
                    Dialog("Sick Leave", "Staff2", "23/1/24", "I am dead");
                } else {
                    Dialogworker("Leader");
                }
                break;
            case R.id.staff33:
                status = "0";
                if (status == "0") {
                    Dialog("Personal leave", "Staff3", "23/1/24", "I am OK");
                } else {
                    Dialogworker("Leader");
                }
                break;
        }
    }



    public void Dialogworker (String name){
        final Dialog dialog = new Dialog(DashBoardActivity.this);
        dialog.setContentView(R.layout.dialogworker);
        dialog.setCancelable(false);

        TextView nametext = dialog.findViewById(R.id.nameperson);
        nametext.setText(name);

        TextView okbutton = dialog.findViewById(R.id.okinworker);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public void Dialog (final String type , final String name , final String day , final String note){
        final Dialog dialog = new Dialog(DashBoardActivity.this);
        dialog.setContentView(R.layout.dialocheck_leave);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView typeleave = dialog.findViewById(R.id.type_leave_check);
        TextView FnameLname = dialog.findViewById(R.id.fname_lname_leave_check);
        TextView dayleave = dialog.findViewById(R.id.day_leave_check);
        TextView notesomething = dialog.findViewById(R.id.note_somting);
        typeleave.setText(type);
        FnameLname.setText(name);
        dayleave.setText(day);
        notesomething.setText(note);
        TextView okbutton = dialog.findViewById(R.id.okbuytton);


        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }
}
