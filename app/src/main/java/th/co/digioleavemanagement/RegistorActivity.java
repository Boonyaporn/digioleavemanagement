package th.co.digioleavemanagement;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import static com.github.mikephil.charting.charts.Chart.LOG_TAG;

public class RegistorActivity extends AppCompatActivity {

    EditText email_registor , password_registor , confirm_registor , firstname_registor , lastname_registor , id_registor ;
    String sex_registor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registor_page);

        into();

    }

    public void into(){
        email_registor = findViewById(R.id.editText_email_registor);
        password_registor = findViewById(R.id.editText_password_registor);
        confirm_registor = findViewById(R.id.editText_confirmpassword_registor);
        firstname_registor = findViewById(R.id.editText_firstname_registor);
        lastname_registor = findViewById(R.id.editText_lastname_registor);
        id_registor = findViewById(R.id.editText_id_registor);
    }



    public void click_female(View view) {
        ImageView female = findViewById(R.id.imageView_female);
        ImageView male = findViewById(R.id.imageView_male);

        male.setBackgroundResource(R.drawable.bordersex);
        male.setImageResource(R.drawable.male_image);

        female.setBackgroundResource(R.drawable.bordersex_onclick);
        female.setImageResource(R.drawable.female_image_onclick);

        Drawable background = female.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ContextCompat.getColor(this, R.color.female_color));

        sex_registor = "female";
    }

    public void click_male(View view) {
        ImageView female = findViewById(R.id.imageView_female);
        ImageView male = findViewById(R.id.imageView_male);

        female.setBackgroundResource(R.drawable.bordersex);
        female.setImageResource(R.drawable.female_image);

        male.setBackgroundResource(R.drawable.bordersex_onclick);
        male.setImageResource(R.drawable.male_image_onclick);

        Drawable background = male.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(ContextCompat.getColor(this, R.color.male_color));


        sex_registor = "male";
    }

    public void click_confirm(View view) {
        String email =  email_registor.getText().toString();
        String password =  password_registor.getText().toString();
        String confirm =  confirm_registor.getText().toString();
        String firstname =  firstname_registor.getText().toString();
        String lastname =  lastname_registor.getText().toString();
        String id =  id_registor.getText().toString();


        if(password.equals(confirm)) {
            Intent intent = new Intent(this, LoginActivith.class);
            startActivity(intent);
        }
        else {
            
        }

    }
}
