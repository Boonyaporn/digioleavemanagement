package th.co.digioleavemanagement;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class SearchEmployeeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView bottomNavigationView;
    BarChart barChart ;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_employee);

        SettingMenu();
        barChart = findViewById(R.id.barchart);

        createBarChart();

    }

    private ArrayList<BarEntry> barEntries()
    {
        ArrayList<BarEntry> barEntries = new ArrayList<>();

        for (int i = 1 ; i <= 12 ; i++){
            int random = (int)(Math.random() * 7 + 1);
            barEntries.add(new BarEntry(i,random));
        }
        return barEntries;
    }


    public void createBarChart(){
        BarDataSet barDataSet1 = new BarDataSet(barEntries(),"Sick Leave");
        barDataSet1.setColor(getResources().getColor(R.color.sick_color));
        BarDataSet barDataSet3 = new BarDataSet(barEntries(),"Vacation Leave");
        barDataSet3.setColor(getResources().getColor(R.color.vacation_color));
        BarDataSet barDataSet2 = new BarDataSet(barEntries(),"Personal Leave");
        barDataSet2.setColor(getResources().getColor(R.color.personal_color));
        BarDataSet barDataSet4 = new BarDataSet(barEntries(),"Other");
        barDataSet4.setColor(getResources().getColor(R.color.other_color));


        BarData data = new BarData(barDataSet1,barDataSet2,barDataSet3,barDataSet4);
        barChart.setData(data);

        String[] month =  new String[] {"January" , "February" , "March" , "April" , "May" , "June" , "July" , "August" , "September" , "October" , "November" , "December"};

        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(month));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.TOP);

        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);

        barChart.setDragEnabled(true);
        barChart.setVisibleXRangeMaximum(3);

        float barSpace = 0.03f;
        float groupSpace = 0.56f;
        data.setBarWidth(0.08f);

        //set Size XAxis
        barChart.getXAxis().setAxisMinimum(0);
        barChart.getXAxis().setAxisMaximum(0+barChart.getBarData().getGroupWidth(groupSpace,barSpace)*12);
        barChart.getAxisLeft().setAxisMinimum(0);

        barChart.groupBars(0,groupSpace,barSpace);
        barChart.invalidate();

        barChart.groupBars(0,groupSpace,barSpace);
        barChart.invalidate();

    }

    public void SettingMenu(){
        bottomNavigationView = findViewById(R.id.nev_bar);
        bottomNavigationView.setItemIconTintList(null);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem =   menu.findItem(R.id.search_employee_nev);
        menuItem.setIcon(getResources().getDrawable(R.drawable.search_employee_onclick));
        menuItem.setEnabled(true);


        if (RoleDemo.getInstance().getRole().equals("HR")){
            bottomNavigationView.getMenu().removeItem(R.id.myTeam_nev);
        }
        else if (RoleDemo.getInstance().getRole().equals("TeamLead")){
            bottomNavigationView.getMenu().removeItem(R.id.search_employee_nev);
        }
        else {
            bottomNavigationView.getMenu().removeItem(R.id.search_employee_nev);
            bottomNavigationView.getMenu().removeItem(R.id.notification_nev);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.dashBoard_nev:
                Intent intent = new Intent(SearchEmployeeActivity.this, DashBoardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case R.id.search_employee_nev:
                Intent intent5 = new Intent(SearchEmployeeActivity.this, SearchEmployeeActivity.class);
                intent5.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent5);
                break;
            case R.id.myTeam_nev:
                Intent intent2 = new Intent(SearchEmployeeActivity.this, MyTeamActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent2);
                break;
            case R.id.notification_nev:
                Intent intent3 = new Intent(SearchEmployeeActivity.this, RequestNotificationActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent3);
                break;
            case R.id.profile_nev:
                Intent intent4 = new Intent(SearchEmployeeActivity.this, ProfileActivity.class);
                intent4.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent4);
                break;
        }

        return false;
    }
}
