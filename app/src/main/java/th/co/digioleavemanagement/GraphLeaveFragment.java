package th.co.digioleavemanagement;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GraphLeaveFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GraphLeaveFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GraphLeaveFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    BarChart barChart;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public GraphLeaveFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GraphLeaveFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GraphLeaveFragment newInstance(String param1, String param2) {
        GraphLeaveFragment fragment = new GraphLeaveFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_graph_leave, container, false);
        barChart = view.findViewById(R.id.barchart_profile);
        createBarChart();
        return view;
    }

    private ArrayList<BarEntry> barEntries()
    {
        ArrayList<BarEntry> barEntries = new ArrayList<>();

        for (int i = 1 ; i <= 12 ; i++){
            int random = (int)(Math.random() * 7 + 1);
            barEntries.add(new BarEntry(i,random));
        }
        return barEntries;
    }


    public void createBarChart(){
        BarDataSet barDataSet1 = new BarDataSet(barEntries(),"Sick Leave");
        barDataSet1.setColor(getResources().getColor(R.color.sick_color));
        BarDataSet barDataSet3 = new BarDataSet(barEntries(),"Vacation Leave");
        barDataSet3.setColor(getResources().getColor(R.color.vacation_color));
        BarDataSet barDataSet2 = new BarDataSet(barEntries(),"Personal Leave");
        barDataSet2.setColor(getResources().getColor(R.color.personal_color));
        BarDataSet barDataSet4 = new BarDataSet(barEntries(),"Other");
        barDataSet4.setColor(getResources().getColor(R.color.other_color));

        BarData data = new BarData(barDataSet1,barDataSet2,barDataSet3,barDataSet4);
        barChart.setData(data);

        String[] month =  new String[] {"January" , "February" , "March" , "April" , "May" , "June" , "July" , "August" , "September" , "October" , "November" , "December"};
        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(month));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.TOP);

        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);

        barChart.setDragEnabled(true);
        barChart.setVisibleXRangeMaximum(3);

        float barSpace = 0.03f;
        float groupSpace = 0.56f;
        data.setBarWidth(0.08f);

        barChart.getXAxis().setAxisMinimum(0);
        barChart.getXAxis().setAxisMaximum(0+barChart.getBarData().getGroupWidth(groupSpace,barSpace)*12);
        barChart.getAxisLeft().setAxisMinimum(0);

        barChart.groupBars(0,groupSpace,barSpace);
        barChart.invalidate();
    }


}
