package th.co.digioleavemanagement;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button bt_hr , bt_teamlead , bt_staff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt_hr = findViewById(R.id.bt_hr);
        bt_staff = findViewById(R.id.bt_staff);
        bt_teamlead = findViewById(R.id.bt_teamLead);

        bt_teamlead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoleDemo.getInstance().setRole("TeamLead");
                startActivity(new Intent(MainActivity.this, LoginActivith.class));

            }
        });

        bt_hr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoleDemo.getInstance().setRole("HR");
                startActivity(new Intent(MainActivity.this, LoginActivith.class));
            }
        });

        bt_staff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoleDemo.getInstance().setRole("Staff");
                startActivity(new Intent(MainActivity.this, LoginActivith.class));
            }
        });


    }
}
