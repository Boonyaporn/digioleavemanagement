package th.co.digioleavemanagement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class LoginActivith extends AppCompatActivity {

    EditText email_login , password_login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);

    }

    public void into(){
        email_login = findViewById(R.id.editText_email_login);
        password_login = findViewById(R.id.editText_password_login);

    }

    public void click_login_button(View view) {
        Intent intent = new Intent(this, DashBoardActivity.class);
        startActivity(intent);
    }

    public void click_registor_button(View view) {
        Intent intent = new Intent(this, RegistorActivity.class);
        startActivity(intent);

    }
}
